﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{


    public class GameOfLife
    {
        int[,] oldBoard;
        int[,] newBoard;
        int rozmiar;
        int zmiana;
        int poczatkowaPopulacja;
        int aktualnaPopulacja;

        public GameOfLife(int rozmiar_)
        {
            rozmiar = rozmiar_;
            zmiana = 0;
            for (int i = 0; i < rozmiar; i++)
            {
                oldBoard = new int[rozmiar, rozmiar];
                newBoard = new int[rozmiar, rozmiar];

            }
        }

        public void prepareBoard()
        {
            
            Random random = new Random();
            for (int i = 0; i < rozmiar; i++)
            {
                for (int j = 0; j < rozmiar; j++)
                {
                    int first = random.Next(0, 2);
                    int second = random.Next(0, 2);
                    if (first < second)
                        oldBoard[i, j] = first;
                    else
                        oldBoard[i, j] = second;
                    if (oldBoard[i, j] == 1)
                        poczatkowaPopulacja++;
                }
            }
            aktualnaPopulacja = poczatkowaPopulacja;
        }

        public void showBoard()
        {
            char[] symbole = { '.', '0'};
            for (int i = 0; i < rozmiar; i++)
            {
                for (int j = 0; j < rozmiar; j++)
                {   
                    Console.Write(symbole[oldBoard[i, j]]);
                    Console.Write(" ");
                }
                Console.WriteLine() ;
            }
            Console.WriteLine();
            string wyjscie = "Poczatkowa populacja: " + poczatkowaPopulacja.ToString() + "\nAktualna populacja: " + aktualnaPopulacja.ToString();
            Console.WriteLine(wyjscie);



        }

        public int countNeighbours(int x, int y)
        {
            int neighbours = 0;
            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    int col = (x + i + rozmiar) % rozmiar;
                    int row = (y + j + rozmiar) % rozmiar;
                    neighbours += oldBoard[col, row];
                }
            }
            neighbours -= oldBoard[x, y];
            return neighbours;
        }

        public void updateBoard()
        {
            aktualnaPopulacja = 0;
            this.zmiana = 0;
            int neighbours = 0;
            for (int i = 0; i < rozmiar; i++)
            {
                for (int j = 0; j < rozmiar; j++)
                {
                    int stan = this.oldBoard[i, j];
                    neighbours = this.countNeighbours(i, j);

                    if ((neighbours < 2 || neighbours > 3) && stan == 1)
                    {
                        newBoard[i,j] = 0;
                        this.zmiana++;
                    }

                    else if (neighbours == 3 && stan == 0)
                    {
                        newBoard[i,j] = 1;
                        this.zmiana++;
                    }

                    else
                    {
                        newBoard[i, j] = stan;
                    }
                         
                    neighbours = 0;
                }
            }

            for (int i = 0; i < rozmiar; i++)
            {
                for (int j = 0; j < rozmiar; j++)
                {
                    oldBoard[i, j] = newBoard[i, j];
                    if (oldBoard[i, j] == 1)
                        aktualnaPopulacja++;
                        
                }
            }
        }


        public void beginGame()
        {
            do
            {
                Console.Clear();
                this.updateBoard();
                this.showBoard();
                System.Threading.Thread.Sleep(1000/15);
                //Console.ReadKey();
                  
            }
            while (this.zmiana != 0);
            Console.WriteLine("Koniec gry");
            Console.ReadKey();
        }

        public void play()
        {
            prepareBoard();
            showBoard();
            Console.WriteLine("Nacisnij klawisz aby rozpoczac!");
            Console.ReadKey();
            Console.Clear();
            beginGame();
        }


    }


    class Program
    {

        static void Main(string[] args)
        {
            int size = 25;
            GameOfLife gra = new GameOfLife(size);
            gra.play();
            
        }
    }
}
